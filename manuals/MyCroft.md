# MyCroft Config

## Installation

0.- Select MyCroft in http://qbo.local:8000/

1.- Look at Qbo and ask mycroft for the pairing code saying:

    pair my device

Write the code that you'll hear.

2.- Go to https://mycroft.ai and login.

3.- Up-right click on the arrow next to your name and select "Devices", after that add a new device. Here you need to introduce your code obtained by Qbo/Mycroft.

## Usage

If you look at Qbo, MyCroft will trigger.
