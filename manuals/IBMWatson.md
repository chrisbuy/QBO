# Install IBM Watson

In order to follow this manual it is necessary that you have an account in IBM Cloud. If you do not have it, [click here](https://www.ibm.com/account/reg/signup?formid=urx-19776) to create one.

If you already have an account you can continue with the next point.

Notice: All the services activated during this manual have a free plan.

## Text to Speech

Access the following link with your IBM Cloud account: [https://console.bluemix.net/catalog/services/text-to-speech](https://console.bluemix.net/catalog/services/text-to-speech)

Define a name for the service and select a region. If you want to use the free plan, select the region of the United Kingdom.

Next, select the plan you want to use. Remember that if you select the free plan, the service will be deleted after 30 days of inactivity.

Finally, click on the create button.

Next you can see the service console and the hidden access credentials. Click on "Show" to see the credentials.

Now, access the QBO panel and in the settings enter these credentials in the corresponding fields and press on the save button.

## Speech to Text

Access the following link with your IBM Cloud account: [https://console.bluemix.net/catalog/services/speech-to-text](https://console.bluemix.net/catalog/services/speech-to-text)

Define a name for the service and select a region. If you want to use the free plan, select the region of the United Kingdom.

Next, select the plan you want to use. Remember that if you select the free plan, the service will be deleted after 30 days of inactivity.

Finally, click on the create button.

Next you can see the service console and the hidden access credentials. Click on "Show" to see the credentials.

Now, access the QBO panel and in the settings enter these credentials in the corresponding fields and press on the save button.

Additionally, You can also modify the listening time that QBO will be listening when the robot hears the hotword or detects its face.

## Watson Assistant

Access the following link with your IBM Cloud account: [https://console.bluemix.net/catalog/services/watson-assistant-formerly-conversation](https://console.bluemix.net/catalog/services/watson-assistant-formerly-conversation)

Define a name for the service and select a region. If you want to use the free plan, select the region of the United Kingdom.

Next, select the plan you want to use. Remember that if you select the free plan, the service will be deleted after 30 days of inactivity.

Finally, click on the create button.

Next you can see the service console and the hidden access credentials. Click on "Show" to see the credentials.

Now, access the QBO panel and in the settings enter these credentials in the corresponding fields and press on the save button.

Then go back to the web where you see the credentials and click on "Start tool" then on the Workspaces tab and proceed to create a new Workspace for your QBO robot.

Define a name, description and language. (Remember that in the QBO configuration you must specify the same language.)

Configure and set the Workspace. (You can look at the IBM Watson catalog)

Once ready, access the Deploy option in the menu and search Workspace ID. Enter it in the corresponding section of the QBO web configuration and restart the robot.

After restarting, QBO will respond with the Intents configured in the Watson Workspace.

## Visual Recognition

Access the following link with your IBM Cloud account: [https://console.bluemix.net/catalog/services/visual-recognition](https://console.bluemix.net/catalog/services/visual-recognition)

Define a name for the service and select a region. If you want to use the free plan, select the region of the EE.UU. sur.

Next, select the plan you want to use. Remember that if you select the free plan, the service will be deleted after 30 days of inactivity.

Finally, click on the create button.

Next you can see the service console and the hidden access credentials. Click on "Show" to see the credentials.

Now, access the QBO panel and in the settings enter these credentials in the corresponding fields and press on the save button.
