#!/usr/bin/env python2
# -*- coding: latin-1 -*-

import subprocess
import wave
import pyaudio
import tempfile
import yaml

from watson_developer_cloud import SpeechToTextV1
from watson_developer_cloud import TextToSpeechV1
from watson_developer_cloud import AssistantV1


class QBOWatson:

	def __init__(self):
		self.config = yaml.safe_load(open("/opt/qbo/config.yml"))
		self.strAudio = ""
		self.GetAudio = False
		self.Response = "hello"
		self.GetResponse = False

		self.FORMAT = pyaudio.paInt16
		self.CHANNELS = 1
		self.RATE = 16000
		self.CHUNK = 1024
		self.RECORD_SECONDS = self.config['SpeechToTextListeningTime']

		self.text_to_speech = TextToSpeechV1(
			username=self.config['TextToSpeechUsername'],
			password=self.config['TextToSpeechPassword'])

		self.speech_to_text = SpeechToTextV1(
			username=self.config['SpeechToTextUsername'],
			password=self.config['SpeechToTextPassword'])

		self.assistant = AssistantV1(
			username=self.config['AssistantUsername'],
			password=self.config['AssistantPassword'],
			version='2017-04-21')

		self.assistantWorkspace = self.config['AssistantWorkspace']

	def listen(self):

		audio = pyaudio.PyAudio()
		stream = audio.open(format=self.FORMAT, channels=self.CHANNELS, rate=self.RATE, input=True, frames_per_buffer=self.CHUNK)

		print "recording..."
		frames = []
		for i in range(0, int(self.RATE / self.CHUNK * self.RECORD_SECONDS)):
			data = stream.read(self.CHUNK)
			frames.append(data)
		print "finished recording"

		# stop Recording
		stream.stop_stream()
		stream.close()
		audio.terminate()

		# Create wav file
		tmp = tempfile.NamedTemporaryFile()
		waveFile = wave.open(tmp, 'wb')
		waveFile.setnchannels(self.CHANNELS)
		waveFile.setsampwidth(audio.get_sample_size(self.FORMAT))
		waveFile.setframerate(self.RATE)
		waveFile.writeframes(b''.join(frames))
		waveFile.close()

		model = 'en-US_BroadbandModel'
		if self.config['language'] == 'spanish':
			model = 'es-ES_BroadbandModel'

		with open(tmp.name) as audio_file:
			results = self.speech_to_text.recognize(audio=audio_file, content_type='audio/wav', model=model)
			if len(results['results']) != 0 and len(results['results'][0]['alternatives']) != 0:
				self.strAudio = results['results'][0]['alternatives'][0]['transcript']
			else:
				self.strAudio = " "
			self.GetAudio = True
			self.askToAssistant(self.strAudio)

	def askToAssistant(self, text):

		response = self.assistant.message(workspace_id=self.assistantWorkspace, input={'text': text})

		print("Watson Assistant Response: %s" % (response['output']['text'][0]))

		self.Response = response['output']['text'][0].encode("utf-8")
		self.GetResponse = True

		return self.Response

	def StartBack(self):
		self.listen()
		return 0

	def SpeechText(self, text):

		voice = 'en-US_MichaelVoice'
		if self.config['language'] == 'spanish':
			voice = 'es-ES_EnriqueVoice'

		with open('/opt/qbo/sounds/watson.wav', 'wb') as audio_file:
			audio_file.write(self.text_to_speech.synthesize(text, accept='audio/wav', voice=voice).content)

		subprocess.call('aplay -D convertQBO /opt/qbo/sounds/watson.wav', shell=True)
